# Magical Surges

Magical Surges a API for Roll20 for the "The Net Libram of Random Magical Effects" version 2. 


**Syntax:** 
!MagicalSurge - Output to chat for all to see.
!GMMagicalSurge - Outputs so only the GM can see. 